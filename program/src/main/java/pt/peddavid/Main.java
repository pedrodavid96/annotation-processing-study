package pt.peddavid;

import pt.peddavid.preprocessor.RequestMethod;
import pt.peddavid.preprocessor.annotation.GetMapping;
import pt.peddavid.preprocessor.annotation.PostMapping;
import pt.peddavid.preprocessor.annotation.RequestMapping;
import pt.peddavid.mappings.Factory;

public class Main {
    public static void main(String[] args) {
        Factory.test2CheckAndCall();
    }

    @GetMapping(path = "test/path", value = "test")
    public static void test() {
    }

    @PostMapping(path = "test/path", value = "test")
    public static void test1() {
    }

    @RequestMapping(path = "test/path", method = RequestMethod.GET)
    public static void test2() {
        System.out.println("Test 2 was called");
    }
}
