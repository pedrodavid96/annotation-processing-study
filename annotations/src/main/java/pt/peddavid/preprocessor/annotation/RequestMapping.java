package pt.peddavid.preprocessor.annotation;

import pt.peddavid.preprocessor.RequestMethod;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.SOURCE)
public @interface RequestMapping {
    String path() default "";
    RequestMethod method();
}
