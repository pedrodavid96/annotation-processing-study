package pt.peddavid.preprocessor.annotation;

import pt.peddavid.preprocessor.RequestMethod;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.SOURCE)
@RequestMapping(method = RequestMethod.TRACE)
public @interface TraceMapping {
    String value();
    String path();
}
