package pt.peddavid.preprocessor;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import pt.peddavid.preprocessor.annotation.GetMapping;
import pt.peddavid.preprocessor.annotation.RequestMapping;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.util.Set;

public class Processor extends AbstractProcessor {

    private Messager messager;
    private Filer filer;


    @Override
    public synchronized void init(ProcessingEnvironment env) {
        this.messager = env.getMessager();
        this.filer = env.getFiler();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment env) {
        annotations.forEach(an -> messager.printMessage(Diagnostic.Kind.NOTE, an.getQualifiedName()));
        for(Element element : env.getElementsAnnotatedWithAny(Set.of(RequestMapping.class))) {
            // messager.printMessage(Diagnostic.Kind.NOTE, element.getSimpleName());
            ExecutableElement method = (ExecutableElement) element;
            TypeElement element1 = (TypeElement) element.getEnclosingElement();

            MethodSpec testMethod = null;
            try {
                testMethod = MethodSpec.methodBuilder(element.getSimpleName() + "CheckAndCall")
                        .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                        .returns(void.class)
                        .addStatement("$T.$L()", ClassName.get(element1), method.getSimpleName())
                        .build();
            } catch (Exception e) {
                e.printStackTrace();
            }
            TypeSpec testType = TypeSpec.classBuilder("Factory")
                    .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                    .addMethod(testMethod)
                    .build();
            try {
                JavaFile.builder("pt.peddavid.mappings", testType)
                        .build()
                        .writeTo(filer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Set.of(RequestMapping.class.getCanonicalName());
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }
}
